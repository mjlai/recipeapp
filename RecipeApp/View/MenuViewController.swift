//
//  MenuViewController.swift
//  RecipeApp
//
//  Created by Tinnolab iOS on 08/02/2020.
//  Copyright © 2020 MJ Lai. All rights reserved.
//

import UIKit

extension MenuViewController {
    //* ---------------------------------------------------
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateRecipeSegue" {
            let vc = (segue.destination as? CreateRecipeViewController)!
            vc.recipeTypes = self.recipeTypes
        }
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    //* ---------------------------------------------------
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    //* ---------------------------------------------------
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeListTableViewCell", for: indexPath) as! RecipeListTableViewCell
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 10, width: cell.frame.width, height: cell.frame.height - 10))
        imageView.contentMode = .scaleAspectFill
 
        let recipe = recipes[indexPath.row]
        cell.label_Title.text = recipe.title
        imageView.load(url: URL(string: recipe.imageUrl) ?? URL(string: "https://dapp.dblog.org/img/default.jpg")!)
        
        cell.backgroundView = UIView()
        cell.backgroundView!.addSubview(imageView)
        
        return cell
    }
    
    //* ---------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0;//Choose your custom row height
    }
    
    //* ---------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRecipe = recipes[indexPath.row]

        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecipeDetailsViewController") as? RecipeDetailsViewController
        vc?.recipe = self.selectedRecipe
        self.navigationController?.pushViewController(vc!, animated: true)
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    //* ---------------------------------------------------
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    //* ---------------------------------------------------
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            removeRecipe(recipe: recipes[indexPath.row])
            
            self.loadAllRecipe()
            self.getRecipeTypeList()
            self.label_NumOfRecipe.text = "\(recipes.count) recipe"
            self.tableView_RecipeList.reloadData()
        }
    }
    
    //* ---------------------------------------------------
    func removeRecipe(recipe: Recipe) {
        recipe.isDeleted = true
        self.recipeController.addOrUpdateRecipeToLocalDb(recipe: recipe)
    }
}

extension MenuViewController : UIPickerViewDataSource, UIPickerViewDelegate {
    //* ---------------------------------------------------
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //* ---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return recipeTypes.count
    }
    
    //* ---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return recipeTypes[row]
    }
    
    //* ---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedType = recipeTypes[row]
        text_Type.text = selectedType
        pickerView.isHidden = true
        
        loadAllRecipe()
        
        if selectedType != "All" {
            var selectedTypeRecipes = [Recipe]()
            for recipe in recipes {
                if recipe.type == selectedType {
                    selectedTypeRecipes.append(recipe)
                }
            }
            recipes.removeAll()
            recipes = selectedTypeRecipes
        }
        
        
        self.tableView_RecipeList.reloadData()
        self.label_NumOfRecipe.text = "\(recipes.count) recipe"
        self.isPickerShow = false
    }
}

class MenuViewController: UIViewController, XMLParserDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var text_Type: UITextField!
    @IBOutlet weak var label_NumOfRecipe: UILabel!
    @IBOutlet weak var btn_SelectType: UIButton!
    @IBOutlet weak var tableView_RecipeList: UITableView!
    
    var recipes = [Recipe]()
    var elementName: String = String()
    var recipeTitle = String()
    var recipeType = String()
    var ingredient = String()
    var time = String()
    var imageUrl = String()
    var method = String()
    
    var recipeTypes = [String]()
    var selectedType = ""
    
    var selectedRecipe: Recipe?
    
    var picker = UIPickerView()
    var isPickerShow = false
    
    internal let recipeController = RecipeController()
    
    //* ---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround() 
    }
    
    //* ---------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        loadAllRecipe()
        getRecipeTypeList()
        self.label_NumOfRecipe.text = "\(recipes.count) recipe"
        self.tableView_RecipeList.reloadData()
    }
    
    //* ---------------------------------------------------
    func loadAllRecipe() {
        self.loadRecipeFromLocalDb()

        if let path = Bundle.main.url(forResource: "recipetypes", withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
        
        //Filter to show undeleted recipe
        recipes = recipes.filter {$0.isDeleted == false}
    }
    
    //* ---------------------------------------------------
    func getRecipeTypeList() {
        if !recipeTypes.contains("All") {
            self.recipeTypes.append("All")
        }
        
        for recipe in recipes {
            if !self.recipeTypes.contains(recipe.type) {
                self.recipeTypes.append(recipe.type)
            }
        }
    }
    
    //* ---------------------------------------------------
    public func loadRecipeFromLocalDb() {
        guard let offlineSegments = recipeController.fetchOfflineCredential() else {
            #if DEBUG
            print("No offline data.")
            #endif
            
            return
        }
        
        self.recipes = offlineSegments
    }
    
    //MARK: Create Picker View
    //* ---------------------------------------------------
    func createPickerView() {
        // UIPickerView
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.backgroundColor = .lightGray
        picker.dataSource = self
        picker.delegate = self
        picker.isHidden = false
        view.addSubview(picker)
        self.isPickerShow = true

        picker.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        picker.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        picker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

    //MARK: XML Parser
    //* ---------------------------------------------------
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if elementName == "recipe" {
            recipeTitle = String()
            recipeType = String()
            ingredient = String()
            time = String()
            imageUrl = String()
            method = String()
        }

        self.elementName = elementName
    }

    //* ---------------------------------------------------
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "recipe" {
            let recipe = Recipe(title: recipeTitle, type: recipeType, ingredient: ingredient, isDeleted: false, imageUrl: imageUrl, method: method)
            
            if !recipes.contains(where: { recipe in recipe.title == recipeTitle}) {
                recipes.append(recipe)
            }
        }
    }

    //* ---------------------------------------------------
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if (!data.isEmpty) {
            if self.elementName == "title" {
                recipeTitle += data
            } else if self.elementName == "type" {
                recipeType += data
            } else if self.elementName == "ingredient" {
                ingredient += data
            } else if self.elementName == "time" {
                time += data
            } else if self.elementName == "imageUrl" {
                imageUrl += data
            } else if self.elementName == "method" {
                method += data
            }
        }
    }
    
    //* MARK: IBAction
    //* ---------------------------------------------------
    @IBAction func barBtnCreateRecipeClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "CreateRecipeSegue", sender: nil)
    }
    
    //* ---------------------------------------------------
    @IBAction func btnSelectRecipeTypeClicked(_ sender: Any) {
        if !isPickerShow {
            self.createPickerView()
        }
        else {
            self.picker.isHidden = true
            self.isPickerShow = false
        }
    }
    
}
