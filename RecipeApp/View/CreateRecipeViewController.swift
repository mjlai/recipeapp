//
//  CreateRecipeViewController.swift
//  RecipeApp
//
//  Created by Tinnolab iOS on 08/02/2020.
//  Copyright © 2020 MJ Lai. All rights reserved.
//

import UIKit
import Foundation

extension CreateRecipeViewController : UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate {
    //* ---------------------------------------------------
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //* ---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return recipeTypes.count
    }
    
    //* ---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return recipeTypes[row]
    }
    
    //* ---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedType = recipeTypes[row]
        text_Type.text = selectedType
        pickerView.isHidden = true
    }
}

class CreateRecipeViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var text_Title: UITextField!
    @IBOutlet weak var text_Type: UITextField!
    @IBOutlet weak var textView_Ingredients: UITextView!
    @IBOutlet weak var textView_Methods: UITextView!
    @IBOutlet weak var imageView_Food: UIImageView!
    
    public var recipeTypes = [String]()
    
    var selectedType = ""
    var imagePicker = UIImagePickerController()
    
    var newRecipeImageUrl = String()
    
    internal let recipeController = RecipeController()
    
    //* ---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.textView_Ingredients.layer.borderWidth = 0.5
        self.textView_Ingredients.layer.borderColor = UIColor.darkGray.cgColor
        self.textView_Methods.layer.borderWidth = 0.5
        self.textView_Methods.layer.borderColor = UIColor.darkGray.cgColor
        self.imageView_Food.layer.borderWidth = 0.5
        self.imageView_Food.layer.borderColor = UIColor.darkGray.cgColor
        
        self.hideKeyboardWhenTappedAround() 
    }
    
    //MARK: Create Picker View
    //* ---------------------------------------------------
    func createPickerView() {
        // UIPickerView
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.backgroundColor = .lightGray
        picker.dataSource = self
        picker.delegate = self
        view.addSubview(picker)

        picker.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        picker.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        picker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    //* ---------------------------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        createPickerView()
        return false
    }
    
    //* ---------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        guard let imageUrl = info[.imageURL] as? URL else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        newRecipeImageUrl = imageUrl.absoluteString 
        
        self.imageView_Food.image = image
    }
    
    
    
    
    //MARK: IBAction
    //* ---------------------------------------------------
    @IBAction func btnUploadPhotoClicked(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false

            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //* ---------------------------------------------------
    @IBAction func btnSaveRecipeClicked(_ sender: Any) {
        if newRecipeImageUrl == "" {
            let alert = UIAlertController(title: "Alert", message: "Please select a photo in order to proceed.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let newRecipe = Recipe(title: text_Title.text ?? "", type: text_Type.text ?? "All", ingredient: textView_Ingredients.text, isDeleted: false, imageUrl: newRecipeImageUrl, method: textView_Methods.text)
        
        self.recipeController.addOrUpdateRecipeToLocalDb(recipe: newRecipe)
        self.navigationController?.popViewController(animated: true)
    }
    
}
