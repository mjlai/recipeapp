//
//  RecipeDetailsViewController.swift
//  RecipeApp
//
//  Created by Tinnolab iOS on 08/02/2020.
//  Copyright © 2020 MJ Lai. All rights reserved.
//

import UIKit

class RecipeDetailsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView_Food: UIImageView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textView_Ingredient: UITextView!
    @IBOutlet weak var btnUploadPhoto: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var textView_Method: UITextView!
    @IBOutlet weak var btnDelete: UIButton!
    
    public var recipe: Recipe?
    
    var imagePicker = UIImagePickerController()
    var updatedRecipeImageUrl = String()
    
    internal let recipeController = RecipeController()
    
    //* ---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView_Food.load(url: URL(string: recipe!.imageUrl)!)
        self.btnUploadPhoto.isHidden = true
        self.btnDelete.isHidden = true
        
        self.textView_Method.isUserInteractionEnabled = false
        self.textView_Method.text = recipe?.method
        self.textView_Method.isHidden = true
        
        self.textView_Ingredient.isUserInteractionEnabled = false
        self.textView_Ingredient.text = recipe?.ingredient
        self.textView_Ingredient.isHidden = false
        
        self.segmentedControl.tintColor = .white
        
        self.title = recipe?.title
        
        self.hideKeyboardWhenTappedAround() 
    }
    
    //MARK: Image picker
    //* ---------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        guard let imageUrl = info[.imageURL] as? URL else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        updatedRecipeImageUrl = imageUrl.absoluteString
        
        self.imageView_Food.image = image
    }
    
    //MARK: IBAction
    //* ---------------------------------------------------
    @IBAction func segmentedControlClicked(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            textView_Ingredient.isHidden = false
            textView_Method.isHidden = true
        case 1:
            textView_Ingredient.isHidden = true
            textView_Method.isHidden = false
        default:
            break
        }
    }

    //* ---------------------------------------------------
    @IBAction func btnEditClicked(_ sender: Any) {
        if btnEdit.titleLabel?.text == "Edit" {
            self.btnUploadPhoto.isHidden = false
            self.btnDelete.isHidden = false
            self.textView_Method.isUserInteractionEnabled = true
            self.textView_Ingredient.isUserInteractionEnabled = true
            self.btnEdit.setTitle("Done", for: .normal)
        }
        else if btnEdit.titleLabel?.text == "Done" {
            self.btnUploadPhoto.isHidden = true
            self.btnDelete.isHidden = true
            self.textView_Method.isUserInteractionEnabled = false
            self.textView_Ingredient.isUserInteractionEnabled = false
            self.btnEdit.setTitle("Edit", for: .normal)
            self.doneUpdateRecipe()
        }
    }
    
    //* ---------------------------------------------------
    @IBAction func btnUploadPhotoClicked(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false

            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //* ---------------------------------------------------
    @IBAction func btnDeleteClicked(_ sender: Any) {
        recipe!.isDeleted = true
        self.recipeController.addOrUpdateRecipeToLocalDb(recipe: recipe!)
        self.navigationController?.popViewController(animated: true)
    }
    
    //* ---------------------------------------------------
    func doneUpdateRecipe() {
        var updatedImageUrl = updatedRecipeImageUrl
        if updatedRecipeImageUrl == "" {
            updatedImageUrl = recipe!.imageUrl
        }
       
        let newRecipe = Recipe(title: recipe!.title, type: recipe!.type, ingredient: textView_Ingredient.text, isDeleted: false, imageUrl: updatedImageUrl, method: textView_Method.text)
        
        self.recipeController.addOrUpdateRecipeToLocalDb(recipe: newRecipe)
    }
}
