//
//  FileUtility.swift
//  RecipeApp
//
//  Created by Tinnolab iOS on 08/02/2020.
//  Copyright © 2020 MJ Lai. All rights reserved.
//

import Foundation

public extension FileManager {
    class func documentsDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    class func cachesDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    class func getFileSizeInKB(atPath:String) -> Double {
        do {
            let attr = try self.default.attributesOfItem(atPath: atPath)
            let fileSize = attr[FileAttributeKey.size] as! UInt64
            var fileSizeKB = Double(fileSize / 1000)
            fileSizeKB = Double(fileSize / 1000)
            return fileSizeKB
        }
        catch {
            return 0
        }
    }
}

