//
//  RecipeData.swift
//  RecipeApp
//
//  Created by Tinnolab iOS on 09/02/2020.
//  Copyright © 2020 MJ Lai. All rights reserved.
//

import Foundation
import GRDB

public class Recipe : Record {
    public var id: Int64?
    public var title: String
    public var type: String
    public var ingredient: String
    public var isDeleted: Bool
    public var imageUrl: String
    public var method: String
    
    init(title: String, type: String, ingredient:String, isDeleted:Bool, imageUrl:String, method:String) {
        self.title = title
        self.type = type
        self.ingredient = ingredient
        self.isDeleted = isDeleted
        self.imageUrl = imageUrl
        self.method = method
        super.init()
    }
    
    override open class var databaseTableName: String {
        return "Recipe"
    }
    
    required public init(row: Row) {
        id = row["id"]
        title = row["title"]
        type = row["type"]
        ingredient = row["ingredient"]
        isDeleted = row["isDeleted"]
        imageUrl = row["imageUrl"]
        method = row["method"]
        super.init(row: row)
    }
    
    override open func encode(to container: inout PersistenceContainer) {
        container["id"] = id
        container["title"] = title
        container["type"] = type
        container["ingredient"] = ingredient
        container["isDeleted"] = isDeleted
        container["imageUrl"] = imageUrl
        container["method"] = method
    }
    
    override open func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
