//
//  RecipeModule.swift
//  RecipeApp
//
//  Created by Tinnolab iOS on 08/02/2020.
//  Copyright © 2020 MJ Lai. All rights reserved.
//

import Foundation
import GRDB

public class DatabaseQueueBase : NSObject {
    //* ------------------------------------------------------------------
    public var databaseQueue : DatabasePool {
        let documentsUrl = FileManager.documentsDir()
        var configuration = Configuration()
//        configuration.passphrase = "13ea0f20d231df5bb468dc20f2e0yt56"
        
        #if DEBUG
        print("db directory: ", documentsUrl)
        #endif
        
        let dbQueue = try! DatabasePool(path: documentsUrl + "/db_training.sqlite", configuration: configuration)
        
        return dbQueue
    }
}

public class RecipeController : DatabaseQueueBase {

    var dbQueue: DatabasePool!


    //* ------------------------------------------------------------------
    public override init() {
        super.init()
        
        self._loadDataBase()
        self._createSchema()
    }

    //* ------------------------------------------------------------------
    deinit {
        self.dbQueue = nil
    }
    
    //* ------------------------------------------------------------------
     private func _loadDataBase() {
         let directoryURL: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
         
         #if DEBUG
         print("db directory: " + directoryURL.path)
         #endif
         
         if !FileManager.default.fileExists(atPath: directoryURL.path + "/") {
             try! FileManager.default.createDirectory(atPath: directoryURL.path + "/", withIntermediateDirectories: true, attributes: nil)
         }
    
         self.dbQueue = super.databaseQueue
     }
    
    //* ------------------------------------------------------------------
    private func _createSchema() {
        if (self.dbQueue == nil) {
            self.dbQueue = super.databaseQueue
        }
        
        do {
            try dbQueue.write() { db in
                // create training segment table
                let isTrainingSegment = try db.tableExists("Recipe")
                if !isTrainingSegment {
                    try db.create(table: "Recipe") { t in
                        t.column("id", .integer).primaryKey()
                        t.column("title", .text).notNull()
                        t.column("type", .text).notNull()
                        t.column("imageUrl", .text).notNull()
                        t.column("ingredient", .text).notNull()
                        t.column("method", .text).notNull()
                        t.column("isDeleted", .boolean).notNull()
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }
    
    //* ------------------------------------------------------------------
    public func _updateSchema(version:Int) {
        if (self.dbQueue == nil) {
            self.dbQueue = super.databaseQueue
        }
        
    }
    
    //* ------------------------------------------------------------------
    private func addRecipe(title:String, type:String, imageUrl:String, ingredient:String, method:String, isDeleted:Bool) -> Int64? {
        do {
            var recipeId:Int64?
            
            try dbQueue.write { db in
                var recipe:Recipe?
                let titleColumn = Column("title")
                let typeColumn = Column("type")
                
                recipe = try Recipe.filter((titleColumn == title) && (typeColumn == type)).fetchOne(db)
                
                
                if (recipe == nil) {
                    do {
                        let newRecipe = Recipe(title: title, type: type, ingredient: ingredient, isDeleted: isDeleted, imageUrl: imageUrl, method: method)
                        try newRecipe.insert(db)
                        recipeId = newRecipe.id
                    }
                    catch {
                        print (error.localizedDescription)
                        return
                    }
                }
            }
            
            return recipeId
        }
        catch {
            print(error)
        }
        
        return nil
    }
    
    //* ------------------------------------------------------------------
    private func updateRecipe(title:String, type:String, imageUrl:String, ingredient:String, method:String, isDeleted:Bool) {
        do {
            try dbQueue.write { db in
                
                var recipe:Recipe?
                let titleColumn = Column("title")
                let typeColumn = Column("type")
                
                recipe = try Recipe.filter((titleColumn == title) && (typeColumn == type) && (typeColumn == type)).fetchOne(db)
                
                
                if (recipe != nil) {
                    do {
                        recipe?.title = title
                        recipe?.type = type
                        recipe?.imageUrl = imageUrl
                        recipe?.ingredient = ingredient
                        recipe?.method = method
                        recipe?.isDeleted = isDeleted
                        
                        try recipe?.update(db)
                    }
                    catch {
                        return
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }
    
    //* ------------------------------------------------------------------
    public func deleteRecipe(title:String, type:String) {
        do {
            try dbQueue.write { db in
                let titleColumn = Column("title")
                let typeColumn = Column("type")
                
                var recipe:Recipe?
                recipe = try Recipe.filter((titleColumn == title) && (typeColumn == type)).fetchOne(db)
            
                if (recipe != nil) {
                    let lastRecipes = try Recipe.filter((titleColumn == title) && (typeColumn == type)).fetchAll(db)
                    
                    for lastRecipe in lastRecipes {
                        do {
                            try lastRecipe.delete(db)
                        }
                        catch {
                            print(error)
                        }
                    }
                    
                    do {
                        try recipe?.delete(db)
                    }
                    catch {
                        print(error)
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }
    
    //* ------------------------------------------------------------------
    public func fetchOfflineCredential() -> [Recipe]? {
        if self.dbQueue == nil {
            self._loadDataBase()
        }
        
        var recipe:[Recipe]?
        do {
            _ = try dbQueue.read { db in
                recipe = try Recipe.fetchAll(db)
            }
            
            return recipe
        }
        catch {
            print(error)
            return nil
        }
    }
    
    //* ------------------------------------------------------------------
    public func isRecipeFoundInLocalDb(title:String, type:String) -> Bool {
        if self.dbQueue == nil {
            self._loadDataBase()
        }
        
        var recipe:[Recipe]?
        do {
            let titleColumn = Column("title")
            let typeColumn = Column("type")
            let isDeletedColumn = Column("isDeleted")
            
            _ = try dbQueue.read { db in
                recipe = try Recipe.filter((titleColumn == title) && (typeColumn == type) && (isDeletedColumn == false)).fetchAll(db)
            }
            
            if recipe != nil {
                return true
            }
            return false
        }
        catch {
            print(error)
            return false
        }
    }
    
    //* ------------------------------------------------------------------
    public func addOrUpdateRecipeToLocalDb(recipe: Recipe) {
        self._loadDataBase()
        self._createSchema()
        
        let _ = self.addRecipe(title: recipe.title, type: recipe.type, imageUrl: recipe.imageUrl, ingredient: recipe.ingredient, method: recipe.method, isDeleted: recipe.isDeleted)
        self.updateRecipe(title: recipe.title, type: recipe.type, imageUrl: recipe.imageUrl, ingredient: recipe.ingredient, method: recipe.method, isDeleted: recipe.isDeleted)
    }
}
